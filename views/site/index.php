<?php

/** @var yii\web\View $this */
/** @var $events \app\controllers\SiteController */

use yii\helpers\ArrayHelper;

$this->title = 'Мероприятия';
?>
<div class="site-index">
    <div class="container-fluid">
        <h1 class="display-4">
            <?= $this->title ?>
        </h1>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Название</th>
                <th>Организаторы</th>
                <th>Дата проведения</th>
                <th>Описание</th>
            </tr>
            </thead>
            <tbody>
                <?php foreach($events as $event): ?>
                    <tr>
                        <td>
                            <?= $event->title ?>
                        </td>
                        <td>
                            <?= $event->organizers?
                                implode(', <br>', ArrayHelper::getColumn($event->organizers, 'full_name')): '' ?>
                        </td>
                        <td>
                            <?= $event->date ?>
                        </td>
                        <td>
                            <?= $event->description ?>
                        </td>
                    </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
