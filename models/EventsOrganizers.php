<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "events_organizers".
 *
 * @property int $events_id
 * @property int $organizers_id
 *
 * @property Events $events
 * @property Organizers $organizers
 */
class EventsOrganizers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'events_organizers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['events_id', 'organizers_id'], 'required'],
            [['events_id', 'organizers_id'], 'integer'],
            [['events_id', 'organizers_id'], 'unique', 'targetAttribute' => ['events_id', 'organizers_id']],
            [['events_id'], 'exist', 'skipOnError' => true, 'targetClass' => Events::class, 'targetAttribute' => ['events_id' => 'id']],
            [['organizers_id'], 'exist', 'skipOnError' => true, 'targetClass' => Organizers::class, 'targetAttribute' => ['organizers_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'events_id' => 'Events ID',
            'organizers_id' => 'Organizers ID',
        ];
    }
}
