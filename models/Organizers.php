<?php

namespace app\models;

use Yii;
use yii\db\Exception;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "organizers".
 *
 * @property int $id
 * @property string $full_name
 * @property string $email
 * @property string|null $phone
 *
 * @property Events[] $events
 * @property EventsOrganizers[] $eventsOrganizers
 */
class Organizers extends \yii\db\ActiveRecord
{

    public $eventsList;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'organizers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['full_name', 'email'], 'required'],
            [['full_name', 'phone'], 'string', 'max' => 255],
            [['email'], 'email'],
            [['eventsList'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'ФИО',
            'email' => 'Email',
            'phone' => 'Телефон',
            'eventsList' => 'События'
        ];
    }

    /**
     * @throws Exception
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($this->eventsList) {
            $this->isNewRecord ?: EventsOrganizers::deleteAll(['organizers_id' => $this->id]);
            $this->setEventsOrganizers();
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @throws Exception
     */
    public function setEventsOrganizers() {
        foreach ($this->eventsList as $event) {
            $eventsOrganizers = new EventsOrganizers();
            $eventsOrganizers->organizers_id = $this->id;
            $eventsOrganizers->events_id = $event;
            $eventsOrganizers->save();
        }
    }

    /**
     * Gets query for [[Events]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Events::class, ['id' => 'events_id'])->viaTable('events_organizers', ['organizers_id' => 'id']);
    }
}
