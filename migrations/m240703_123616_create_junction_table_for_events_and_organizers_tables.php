<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%events_organizers}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%events}}`
 * - `{{%organizers}}`
 */
class m240703_123616_create_junction_table_for_events_and_organizers_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%events_organizers}}', [
            'events_id' => $this->integer(),
            'organizers_id' => $this->integer(),
            'PRIMARY KEY(events_id, organizers_id)',
        ]);

        // creates index for column `events_id`
        $this->createIndex(
            '{{%idx-events_organizers-events_id}}',
            '{{%events_organizers}}',
            'events_id'
        );

        // add foreign key for table `{{%events}}`
        $this->addForeignKey(
            '{{%fk-events_organizers-events_id}}',
            '{{%events_organizers}}',
            'events_id',
            '{{%events}}',
            'id',
            'CASCADE'
        );

        // creates index for column `organizers_id`
        $this->createIndex(
            '{{%idx-events_organizers-organizers_id}}',
            '{{%events_organizers}}',
            'organizers_id'
        );

        // add foreign key for table `{{%organizers}}`
        $this->addForeignKey(
            '{{%fk-events_organizers-organizers_id}}',
            '{{%events_organizers}}',
            'organizers_id',
            '{{%organizers}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%events}}`
        $this->dropForeignKey(
            '{{%fk-events_organizers-events_id}}',
            '{{%events_organizers}}'
        );

        // drops index for column `events_id`
        $this->dropIndex(
            '{{%idx-events_organizers-events_id}}',
            '{{%events_organizers}}'
        );

        // drops foreign key for table `{{%organizers}}`
        $this->dropForeignKey(
            '{{%fk-events_organizers-organizers_id}}',
            '{{%events_organizers}}'
        );

        // drops index for column `organizers_id`
        $this->dropIndex(
            '{{%idx-events_organizers-organizers_id}}',
            '{{%events_organizers}}'
        );

        $this->dropTable('{{%events_organizers}}');
    }
}
