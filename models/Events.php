<?php

namespace app\models;

use Yii;
use yii\db\Exception;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "events".
 *
 * @property int $id
 * @property string $title
 * @property string|null $date
 * @property string|null $description
 *
 * @property EventsOrganizers[] $eventsOrganizers
 * @property Organizers[] $organizers
 */
class Events extends \yii\db\ActiveRecord
{
    
    public $organizersList;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'events';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['date'], 'date', 'format' => 'php:Y-m-d'],
            [['organizersList'], 'safe'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'date' => 'Дата проведения',
            'organizersList' => 'Организаторы',
            'description' => 'Описание',
        ];
    }

    /**
     * @throws Exception
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($this->organizersList) {
            $this->isNewRecord ?: EventsOrganizers::deleteAll(['events_id' => $this->id]);
            $this->setEventsOrganizers();
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @throws Exception
     */
    public function setEventsOrganizers() {
        foreach ($this->organizersList as $organizerId) {
            $eventsOrganizers = new EventsOrganizers();
            $eventsOrganizers->events_id = $this->id;
            $eventsOrganizers->organizers_id = $organizerId;
            $eventsOrganizers->save();
        }
    }

    /**
     * Gets query for [[Organizers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrganizers()
    {
        return $this->hasMany(Organizers::class, ['id' => 'organizers_id'])->viaTable('events_organizers', ['events_id' => 'id']);
    }
}
